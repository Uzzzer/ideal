<?php

//Auth::routes();
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/register', 'Auth\RegisterController@register')->name('register');

Route::prefix('account')->as('account.')->middleware('user')->group(function () {
    Route::get('/profile', 'AccountController@profile')->name('profile');
});

Route::prefix('admin')->as('admin.')->middleware('admin')->group(function () {
    Route::get('/', 'Admin\DashboardController@index')->name('dashboard');

    Route::prefix('pages')->as('pages.')->group(function () {
        Route::get('/', 'Admin\PageController@index')->name('index');
        Route::get('/create', 'Admin\PageController@create')->name('create');
    });

    Route::prefix('users')->as('users.')->group(function () {
        Route::get('/', 'Admin\UserController@index')->name('index');
        Route::get('/create', 'Admin\UserController@create')->name('create');
    });

    Route::prefix('settings')->as('settings.')->group(function () {
        Route::get('/', 'Admin\SettingController@index')->name('index');
    });

    Route::prefix('menu')->as('menu.')->group(function () {
        Route::get('/', 'Admin\MenuController@index')->name('index');
    });

    Route::prefix('testimonials')->as('testimonials.')->group(function () {
        Route::get('/', 'Admin\TestimonialController@index')->name('index');
    });

    Route::prefix('tests')->as('tests.')->group(function () {
        Route::get('/', 'Admin\TestController@index')->name('index');
    });

    Route::prefix('posts')->as('posts.')->group(function () {
        Route::get('/', 'Admin\PostController@index')->name('index');
        Route::get('/create', 'Admin\PostController@create')->name('create');
    });
});

Route::get('{slug?}', 'PageController@router')->where('slug', '(.*)');