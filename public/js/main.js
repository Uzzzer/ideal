var $ = jQuery.noConflict();

$(document).ready(function () {

    $('#loginForm').on('submit', function(){
        $('#loginForm .invalid-feedback').hide();

        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                window.location.href = response.redirectUrl;
            },
            error: function (response) {
                response = JSON.parse(response.responseText);

                $.each(response.errors, function (field, value) {
                    $('#loginForm .invalid-feedback[data-field="' + field + '"]').text(value[0]).show();
                });
            }
        });

        return false;
    });

    $('#registerForm').on('submit', function(){
        $('#registerForm .invalid-feedback').hide();

        $.ajax({
            url: $(this).attr('action'),
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                window.location.href = '/account/profile';
            },
            error: function (response) {
                response = JSON.parse(response.responseText);

                $.each(response.errors, function (field, value) {
                    $('#registerForm .invalid-feedback[data-field="' + field + '"]').text(value[0]).show();
                });
            }
        });

        return false;
    });

    //accordion
    var accordion = document.createElement('script');
    accordion.src = 'js/accordion.js';
    document.getElementsByTagName('body')[0].appendChild(accordion);

    $('.mob_menu_btn').click(function () {
        $(this).toggleClass("active");
        $('.header_nav').toggleClass("active");
        $('body').toggleClass("menu_open");
        return false;
    });

    var uaTwo = window.navigator.userAgent;
    var isIETwo = /MSIE|Trident/.test(uaTwo);

    if (isIETwo) {
        document.documentElement.classList.add('ie');
    }


    $('.test_qa_s .qa_row .qa_radio_circle').click(function () {
        $('html, body').animate({
            scrollTop: $(this).parent().next().offset().top
        }, 300)
    });

    $(' .test_qa_step_wrpr .btn').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $('.test_qa_s').offset().top
        }, 300)
    })


});
