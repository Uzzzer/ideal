@extends('layouts.main')

@section('content')
    <main class="articles_page">
        <section class="top_section_image top_section_image_2">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="module-content module-content_2 module-content_2_1">
                            <h1>Articles & Videos</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="articles">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8">
                        <div class="module-post">
                            <div class="post">
                                <div class="images">
                                    <img src="/images/3.%20Articles%20_%20Videos/post_1.jpg" alt="">
                                </div>
                                <div class="post_body">
                                    <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing
                                    </a>
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum.

                                    </div>
                                    <div class="wrapper-flex">
                                        <a href="#" class="btn btn_no_bg margin_0_auto">Read more</a>
                                    </div>
                                </div>
                                <div class="post_footer">
                                    <div class="cat">
                                        <span class="title_cat">Categories:</span>
                                        <span class="wrapper">
                                            <a href="#" class="cat_link"> Lorem Ipsum,</a>
                                            <a href="#" class="cat_link"> Lorem Ipsum</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="post">
                                <div class="images">
                                    <img src="/images/3.%20Articles%20_%20Videos/post_2.jpg" alt="">
                                </div>
                                <div class="post_body">
                                    <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing
                                    </a>
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum.

                                    </div>
                                    <div class="wrapper-flex">
                                        <a href="#" class="btn btn_no_bg margin_0_auto">Read more</a>
                                    </div>
                                </div>
                                <div class="post_footer">
                                    <div class="cat">
                                        <span class="title_cat">Categories:</span>
                                        <span class="wrapper">
                                            <a href="#" class="cat_link"> Lorem Ipsum,</a>
                                            <a href="#" class="cat_link"> Lorem Ipsum</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="post">
                                <div class="images">
                                    <img src="/images/3.%20Articles%20_%20Videos/post_3.jpg" alt="">
                                </div>
                                <div class="post_body">
                                    <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing
                                    </a>
                                    <div class="content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum.

                                    </div>
                                    <div class="wrapper-flex">
                                        <a href="#" class="btn btn_no_bg margin_0_auto">Read more</a>
                                    </div>
                                </div>
                                <div class="post_footer">
                                    <div class="cat">
                                        <span class="title_cat">Categories:</span>
                                        <span class="wrapper">
                                            <a href="#" class="cat_link"> Lorem Ipsum,</a>
                                            <a href="#" class="cat_link"> Lorem Ipsum</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="sidebar">
                            <div class="sidebar_item">
                                <h3 class="sidebar_title">Recent Videos</h3>
                                <div class="module-post module-post_1">
                                    <div class="post video">
                                        <div class="images">
                                            <img src="/images/3.%20Articles%20_%20Videos/post_1.jpg" alt="">
                                        </div>
                                        <div class="post_body">
                                            <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="module-post module-post_1">
                                    <div class="post video">
                                        <div class="images">
                                            <img src="/images/3.%20Articles%20_%20Videos/post_1.jpg" alt="">
                                        </div>
                                        <div class="post_body">
                                            <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-flex">
                                    <a href="#" class="btn btn_no_bg margin_0_auto">View all videos</a>
                                </div>
                            </div>

                            <div class="sidebar_item">
                                <h3 class="sidebar_title">Recent Articles</h3>
                                <div class="module-post module-post_1">
                                    <div class="post">
                                        <div class="images">
                                            <img src="/images/3.%20Articles%20_%20Videos/post_1.jpg" alt="">
                                        </div>
                                        <div class="post_body">
                                            <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="module-post module-post_1">
                                    <div class="post">
                                        <div class="images">
                                            <img src="/images/3.%20Articles%20_%20Videos/post_1.jpg" alt="">
                                        </div>
                                        <div class="post_body">
                                            <a href="#" class="title">Lorem ipsum dolor sit amet, consectetur
                                                adipiscing
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="wrapper-flex">
                                    <a href="#" class="btn btn_no_bg margin_0_auto">View all articles</a>
                                </div>
                            </div>
                            <div class="sidebar_item">
                                <h3 class="sidebar_title">Categories</h3>
                                <ul class="categories_list">
                                    <li>
                                        <a href="#">Lorem ipsum</a>
                                    </li>
                                    <ul class="sub_list">
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                    </ul>
                                </ul>

                                <ul class="categories_list">
                                    <li>
                                        <a href="#">Lorem ipsum</a>
                                    </li>
                                    <ul class="sub_list">
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem ipsum</a>
                                        </li>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection