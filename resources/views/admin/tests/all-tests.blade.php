@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="header_s_title_wrpr">
                        <h1 class="m-0 text-dark">Test</h1>
                    </div>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Test</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card-body" id="sortable">
                        <div class="card sortable-card">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col">
                                        <h5 class="mb-4">
                                            <i class="nav-icon fa fa-arrows-alt"></i>
                                            Phase 1
                                        </h5>
                                        <div class="form-group">
                                            <label>Question</label>
                                            <input type="text" class="form-control mb-3" placeholder="Enter ..."
                                                   value="You enjoy vibrant social events with lots of people.">

                                            <input type="text" class="form-control" placeholder="Enter ..."
                                                   value="You often spend time exploring unrealistic yet intriguing ideas.">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-danger float-left">Delete Phase</button>
                                        <button type="submit"
                                                class="btn btn-success ml-1 float-right">Add New Question
                                        </button>
                                        <button type="submit" class="btn  float-right btn-primary">Add row</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card sortable-card">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col">
                                        <h5 class="mb-4">
                                            <i class="nav-icon fa fa-arrows-alt"></i>
                                            Phase 2
                                        </h5>
                                        <div class="form-group">
                                            <label>Question</label>
                                            <input type="text" class="form-control mb-3" placeholder="Enter ..."
                                                   value="Which of the following would give you the greatest satisfaction from a job well done?">
                                            <label>Answer options</label>
                                            <div class="row-group">
                                                <textarea class="form-control mb-1" rows="3" placeholder="Enter ...">Helping people by providing them with more options / choices / variety.
                                                </textarea>
                                                <button class="btn btn-danger btn-sm float-right mb-1">Delete row</button>
                                            </div>
                                            <div class="row-group">
                                                <textarea class="form-control mb-1" rows="3" placeholder="Enter ...">Helping people by reducing hassles / frustrations / annoyances / difficulties.
                                                </textarea>
                                                <button class="btn btn-danger btn-sm float-right">Delete row</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-danger float-left">Delete Phase</button>
                                        <button type="submit"
                                                class="btn btn-success ml-1 float-right">Add New Question
                                        </button>
                                        <button type="submit" class="btn  float-right btn-primary">Add row</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card sortable-card">
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col">
                                        <h5 class="mb-4">
                                            <i class="nav-icon fa fa-arrows-alt"></i>
                                            Phase 3
                                        </h5>
                                        <div class="form-group">
                                            <label>Question</label>
                                            <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-danger float-left">Delete Phase</button>
                                        <button type="submit"
                                                class="btn btn-success ml-1 float-right">Add New Question
                                        </button>
                                        <button type="submit" class="btn  float-right btn-primary">Add row</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Add New Phase</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection