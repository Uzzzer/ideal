@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="header_s_title_wrpr">
                        <h1 class="m-0 text-dark">Settings</h1>
                    </div>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Settings</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card-body">

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="email">
                        </div>

                        <div class="form-group">
                            <label>Facebook</label>
                            <input type="text" class="form-control" placeholder="url">
                        </div>

                        <div class="form-group">
                            <label>YouTube</label>
                            <input type="text" class="form-control" placeholder="url">
                        </div>

                        <div class="form-group">
                            <label>Instagram</label>
                            <input type="text" class="form-control" placeholder="url">
                        </div>

                        <div class="form-group">
                            <label>Twitter</label>
                            <input type="text" class="form-control" placeholder="url">
                        </div>
                        <div class="form-group">
                            <label>Logo</label>
                            <div class="logo_img">
                                <img src="/admin-assets/img/Logo_ideal.svg" alt="">
                            </div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection