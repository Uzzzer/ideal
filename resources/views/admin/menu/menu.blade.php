@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="header_s_title_wrpr">
                        <h1 class="m-0 text-dark">Menu</h1>
                    </div>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Menu</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card-body">

                        <div class="form-group">
                            <label>New menu</label>
                            <input type="text" class="form-control" placeholder="New menu">
                        </div>
                        <div class="form-group">
                            <label>Select menu</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected" disabled>Select menu</option>
                                <option>Header menu</option>
                                <option>Footer footer</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Select a page</label>
                            <select class="select2" multiple="multiple" data-placeholder="Select a page"
                                    style="width: 100%;">
                                <option>Page 1</option>
                                <option>Page 2</option>
                                <option>Page 3</option>
                                <option>Page 4</option>
                                <option>Page 5</option>
                                <option>Page 6</option>
                                <option>Page 7</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Display location</label>
                            <select class="form-control select2" style="width: 100%;">
                                <option selected="selected" disabled>Select location</option>
                                <option>Header</option>
                                <option>Footer</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection