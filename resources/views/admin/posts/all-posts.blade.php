@extends('layouts.admin')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <div class="header_s_title_wrpr">
                        <h1 class="m-0 text-dark">All posts</h1>
                        <a href="{{ route('admin.posts.create') }}" class="btn btn-success btn-lg new_course_btn"><i class="fas fa-plus"></i> New post</a>
                    </div>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All posts</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">All posts</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Post</th>
                                    <th>Edit</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1.</td>
                                    <td>Update software</td>
                                    <td>
                                        <a href="create_edit_post.php" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-edit"></i> Edit</a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-eye"></i> View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2.</td>
                                    <td>Clean database</td>
                                    <td>
                                        <a href="create_edit_post.php" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-edit"></i> Edit</a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-eye"></i> View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3.</td>
                                    <td>Cron job running</td>
                                    <td>
                                        <a href="create_edit_post.php" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-edit"></i> Edit</a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-eye"></i> View</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4.</td>
                                    <td>Fix and squish bugs</td>
                                    <td>
                                        <a href="create_edit_post.php" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-edit"></i> Edit</a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn bg-gradient-primary btn-md"><i
                                                    class="fas fa-eye"></i> View</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <ul class="pagination pagination-sm m-0 float-right">
                                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection