@extends('layouts.main')

@section('content')
    <main>
        <section class="page_content privacy_policy_page">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="header_content">
                            <h1 class="title">Privacy Policy</h1>
                            <div class="last_modified">Last modified: September 30, 2018</div>
                        </div>
                        <div class="body_content">
                            <h2>Lorem ipsum</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Enim tortor at auctor urna nunc id cursus
                                metus. Elementum nisi quis eleifend quam. Suspendisse sed nisi lacus sed viverra tellus
                                in hac. Vitae
                                <a href="#">elementum</a>
                                curabitur vitae nunc sed velit dignissim sodales.
                            </p>
                            <p>Tempus iaculis urna id volutpat lacus. Tempor commodo ullamcorper a lacus vestibulum sed.
                                In dictum non consectetur a. Et odio pellentesque diam volutpat commodo sed. Sem et
                                tortor consequat
                                <a href="#">id porta nibh</a>
                                . Nullam eget felis eget nunc lobortis mattis aliquam
                                faucibus purus.
                            </p>
                            <blockquote>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Enim tortor at auctor urna nunc id cursus
                                metus. Elementum nisi quis eleifend quam. Suspendisse sed nisi lacus sed viverra tellus
                                in hac. Vitae elementum curabitur vitae nunc sed velit dignissim sodales. Tempus iaculis
                                urna id volutpat lacus. Tempor commodo ullamcorper a lacus vestibulum sed. In dictum non
                                consectetur
                            </blockquote>
                            <p>Id porta nibh venenatis cras sed. Non sodales neque sodales ut. Dui id ornare arcu odio
                                ut sem nulla pharetra diam. Augue neque gravida in fermentum et
                                <a href="#">sollicitudin</a>
                                ac. Purus gravida quis blandit turpis cursus. Faucibus a
                                pellentesque sit amet porttitor
                            </p>
                            <h2>Lorem ipsum</h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Enim tortor at auctor urna nunc id cursus
                                metus. Elementum nisi quis eleifend quam.
                                <a href="#">Suspendisse</a>
                                sed nisi lacus sed
                                viverra tellus
                                in hac. Vitae elementum curabitur vitae nunc sed velit dignissim sodales.
                            </p>
                            <p>Tempus iaculis urna id volutpat lacus.
                                <a href="#">Tempor commodo</a>
                                ullamcorper a lacus
                                vestibulum sed.
                                In dictum non consectetur a. Et odio pellentesque diam volutpat commodo sed. Sem et
                                tortor consequat id porta nibh. Nullam eget felis eget nunc lobortis mattis aliquam
                                faucibus purus.
                            </p>
                            <ul>
                                <li>Enim tortor at auctor urna nunc id cursus metus</li>
                                <li>itae elementum curabitur vitae nun</li>
                                <li>ue diam volutpat commodo sed</li>
                                <li>ementum curabitur vitae nunc sed velit dignis</li>
                            </ul>
                            <p>Id porta nibh venenatis cras sed. Non sodales neque sodales ut. Dui id ornare arcu odio
                                ut sem nulla pharetra diam. Augue neque gravida in fermentum et sollicitudin ac. Purus
                                gravida quis blandit turpis cursus. Faucibus a pellentesque sit amet porttitor
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection