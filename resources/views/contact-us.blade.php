@extends('layouts.main')

@section('content')
    <main>
        <section class="top_section_image">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="module-content module-content_1 max-width_660 margin-t-180 margin_l_auto">
                            <h1>How can we help?</h1>
                            <p class="text">
                                Please let us know of any questions or suggestions you may have. Ensuring you have a
                                positive experience is important to us.
                            </p>
                            <a href="mailto:support@idealacity.com" class="mail">support@idealacity.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="accordion_section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title_section">Frequently Asked Questions</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h3 class="title_accordion">Lorem ipsum dolor sit amet</h3>
                        <div class="accordion">
                            <section class="accordion_item">
                                <h3 class="title_block">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                            <section class="accordion_item">
                                <h3 class="title_block">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod
                                </h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                            <section class="accordion_item">
                                <h3 class="title_block">Ut enim ad minim veniam, quis nostrud</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                            <section class="accordion_item">
                                <h3 class="title_block">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                        </div>

                        <h3 class="title_accordion">Lorem ipsum</h3>
                        <div class="accordion">
                            <section class="accordion_item">
                                <h3 class="title_block">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                            <section class="accordion_item">
                                <h3 class="title_block">Lorem ipsum dolor sit amet</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                            <section class="accordion_item">
                                <h3 class="title_block">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</h3>
                                <div class="info">
                                    <p class="info_item">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                        dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                    </p>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection