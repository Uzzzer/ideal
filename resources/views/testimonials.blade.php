@extends('layouts.main')

@section('content')
    <main>
        <section class="top_section_image top_section_image_1">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="module-content module-content_2">
                            <h1>Testimonials</h1>
                            <p class="text">
                                See what people are saying about the Trinity Test
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="people_saying people_saying_1">
            <div class="container">
                <div class="module-card module-card_2">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar.svg" alt="">
                                </div>
                                <div class="title">Mary G.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_2.svg" alt="">
                                </div>
                                <div class="title">John F.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_3.svg" alt="">
                                </div>
                                <div class="title">Jennifer L.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar.svg" alt="">
                                </div>
                                <div class="title">Mary G.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_2.svg" alt="">
                                </div>
                                <div class="title">John F.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_3.svg" alt="">
                                </div>
                                <div class="title">Jennifer L.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar.svg" alt="">
                                </div>
                                <div class="title">Mary G.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_2.svg" alt="">
                                </div>
                                <div class="title">John F.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_3.svg" alt="">
                                </div>
                                <div class="title">Jennifer L.</div>
                                <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                    eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <ul class="pagination">
                            <a href="#" class="prev"></a>
                            <li class="active">
                                <a href="#">1</a>
                            </li>
                            <li>
                                <a href="#">2</a>
                            </li>
                            <li>
                                <a href="#">3</a>
                            </li>
                            <a href="#" class="next"></a>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection