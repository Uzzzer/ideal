@extends('layouts.main')

@section('content')
    <main>
        <section class="top_section">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="module-content max-width_660 margin-t-180">
                            <h1>“It’s Amazing to Finally Look Forward to a Career I Love.”</h1>
                            <p class="text">
                                Take the Trinity test. A new and simple way to discover your ideal career path. One that
                                will give you purpose and passion in your working life and beyond.
                            </p>
                            <a href="#" class="btn btn_344_78 btn_f_s_38">Take the Test</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="people_saying">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title_section">What People Are Saying</h2>
                    </div>
                </div>
                <div class="module-card">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar.svg" alt="">
                                </div>
                                <div class="title">Mary – The Designer</div>
                                <div class="content">“This test helped me find my dream job! I am so much happier now
                                    and I
                                    can’t believe it, but I actually find myself looking forward to Mondays.”
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_2.svg" alt="">
                                </div>
                                <div class="title">John – The Engineer</div>
                                <div class="content">“Wow, this is life changing. I felt so stuck and hopeless before
                                    but immediately after completing this test I knew exactly what I wanted to do with
                                    my life. Incredible!”
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/1.%20Home%20Page/user_avatar_3.svg" alt="">
                                </div>
                                <div class="title">Jennifer – The Writer</div>
                                <div class="content">“It’s priceless! To find a career path that fits who I want to be
                                    is really inspiring.” I finally have purpose now and I’m truly excited about the
                                    future.”
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <a href="#" class="btn btn_344_78 btn_f_s_38 btn_m_b_120_center">Take the Test</a>
                </div>
            </div>
        </section>

        <section class="trinity_test">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title_section">Why You Should Take the Trinity Test</h2>
                    </div>
                </div>
                <div class="module-card-row">
                    <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="item">
                                <h3 class="title">To find a job role that ideally fits you</h3>
                                <blockquote>“Choose a job you love, and you will never have to work a day in your
                                    life.”
                                </blockquote>
                                <div class="author">– Confucius</div>
                                <div class="number">01</div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="image">
                                <img src="/images/1.%20Home%20Page/1.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="item">
                                <h3 class="title">To find work that gives you immense satisfaction</h3>
                                <blockquote>“We were created for meaningful work, and one of life’s greatest pleasures
                                    is the satisfaction of a job well done.”
                                </blockquote>
                                <div class="author">– John C. Maxwell</div>
                                <div class="number">02</div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="image">
                                <img src="/images/1.%20Home%20Page/2.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="item">
                                <h3 class="title">To discover who you ideally want to be</h3>
                                <blockquote>“To be yourself in a world that is constantly trying to make you something
                                    else is the greatest accomplishment.”
                                </blockquote>
                                <div class="author">– Ralph Waldo Emerson</div>
                                <div class="number">03</div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
                            <div class="image">
                                <img src="/images/1.%20Home%20Page/3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="#" class="btn btn_344_78 btn_f_s_38 btn_m_b_120_center">Take the Test</a>
                </div>
            </div>
        </section>

        <section class="trust_us">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title_section">Why Trust Us?</h2>
                    </div>
                </div>
                <div class="module-card module-card_1">
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/Icons/1. Home Page/Research.svg" alt="">
                                </div>
                                <div class="title">Research</div>
                                <div class="content">The Trinity test is based on the analysis of 10’s of thousands of
                                    job profiles across the world in combination with robust and highly accurate testing
                                    techniques.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/Icons/1. Home Page/Exclusivity.svg" alt="">
                                </div>
                                <div class="title">Exclusivity</div>
                                <div class="content">The Trinity test is one of a kind and exclusively offered by
                                    Idealacity.com.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                            <div class="item">
                                <div class="icon">
                                    <img src="/images/Icons/1. Home Page/Satisfaction.svg" alt="">
                                </div>
                                <div class="title">Satisfaction</div>
                                <div class="content">We pride ourselves on 100% customer satisfaction. Idealacity makes
                                    every effort to ensure your experience is so positive you’ll want to tell everyone
                                    about it.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <a href="#" class="btn btn_344_78 btn_f_s_38 btn_m_b_120_center">Take the Test</a>
                </div>
            </div>
        </section>
    </main>
@endsection