@extends('layouts.main')

@section('content')
    <main>
        <section class="profile_menu_bg">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <ul class="profile_menu">
                            <li>
                                <a href="#">
                                    <span class="icon">
                                        <img src="{{ asset('images/svg/profile-overview.svg') }}" />
                                    </span>
                                    Profile overview
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon">
                                        <img src="{{ asset('images/svg/test-results.svg') }}" />
                                    </span>
                                    Test Results
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="icon">
                                        <img src="{{ asset('images/svg/preferences.svg') }}" />
                                    </span>
                                    Preferences
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="info_user">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-5">
                        <div class="image">
                            <img src="/images/2.%20Profile%20page/User%20Profile.png" alt="">
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-7">
                        <div class="info_list">
                            <h1 class="title">Hi, {{ auth()->user()->name }}!</h1>
                            <div class="wrapper_list">
                                <div class="row_list">
                                    <div class="label">Job Role:</div>
                                    <div class="value">Designer</div>
                                </div>
                                <div class="row_list">
                                    <div class="label">Value You Create:</div>
                                    <div class="value">To improve the look</div>
                                </div>
                                <div class="row_list">
                                    <div class="label">Who You Help:</div>
                                    <div class="value">People (Consumers)</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="pdf_box">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="title">Test Results</h2>
                        <div class="sub_title">Download a PDF of your report</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="wrapper-flex">
                            <div class="pdf_item">
                                <div class="item_inner">
                                    <div class="icon">
                                        <img src="/images/pdf.svg" alt="">
                                    </div>
                                    <div class="date">January 18, 2019</div>
                                </div>
                                <a href="#" class="link btn" download="">Download</a>
                            </div>
                            <div class="pdf_item">
                                <div class="item_inner">
                                    <div class="icon">
                                        <img src="/images/pdf.svg" alt="">
                                    </div>
                                    <div class="date">September 10, 2019</div>
                                </div>
                                <a href="#" class="link btn" download="">Download</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="preferences">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="title">Preferences</div>
                        <form action="" class="preferences_form">
                            <label>
                                <span class="wrap_input input_510">
                                    <span class="title_label">Your e-mail address</span>
                                <input type="email" class="input" value="mail@mail.com">
                                </span>
                                <button type="button" class="btn">Edit E-mail</button>
                            </label>

                            <label>
                                <span class="wrap_input input_510">
                                    <span class="title_label">Your name</span>
                                <input type="text" class="input" value="user name">
                                </span>
                                <button type="button" class="btn">Change Name</button>
                            </label>

                            <label>
                                <span class="wrap_input">
                                    <span class="title_label">Your gender</span>
                                <select name="" id="">
                                    <option value="">Male</option>
                                    <option value="">Female</option>
                                </select>
                                </span>
                                <button type="button" class="btn">Change Gender</button>
                            </label>

                            <label class="avatar">
                                <span class="wrap_input">
                                    <span class="title_label">Your avatar</span>
                                    <span class="wrap_btn_img">
                                         <span class="wrap_img">
                                  <img src="/images/user_avatar.svg" alt="">
                              </span>
                                         <button type="button" class="btn">Upload Photo</button>
                                </span>
                                    </span>
                            </label>

                            <label>
                                <span class="wrap_input input_370">
                                    <span class="title_label">Change your password</span>
                                <input type="password" class="input" value="your password">
                                </span>
                                <button type="button" class="btn">Change Password</button>
                            </label>

                            <label class="delete_profile">
                                <span class="wrap_input">
                                    <span class="title_label">Delete your profile</span>
                              <button type="button" class="btn btn_delete">Delete</button>
                                </span>
                            </label>

                        </form>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection