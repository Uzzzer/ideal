<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $page->seo_title ?? $page->title }}</title>
    <meta name="description" content="{{ $page->seo_description }}">

    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css">

    <link rel="stylesheet" href="{{ asset('css/main_anton.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main_kolya.css') }}">

    <link rel="stylesheet" href="{{ asset('css/media_anton.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media_artur.css') }}">
    <link rel="stylesheet" href="{{ asset('css/media_kolya.css') }}">

    <link rel="stylesheet" href="{{ asset('css/additional.css') }}">

    @yield('css-section')
</head>
<body>

<header>
    <div class="container">
        <div class="row">
            <div class="col-3 col-xl-3">
                <div class="logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('images/Logo.svg') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-9 col-xl-9">
                <div class="wrapper-flex-a-center-j-end h-100-percent">
                    <nav class="header_nav">
                        <ul class="menu">
                            <li>
                                <a href="#" class="take_the_test_mob">Take the Test</a>
                            </li>
                            <li>
                                <a href="{{ auth()->check() ? (auth()->user()->isAdmin() ? route('admin.dashboard') : route('account.profile')) : route('account.profile') }}"
                                   {!! !auth()->check() ? 'data-fancybox data-src="#login_popup"' : '' !!}>
                                    {{ (auth()->check() and auth()->user()->isAdmin()) ? 'Dashboard' : 'My profile' }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/articles-and-videos') }}">Articles & Videos</a>
                            </li>
                            <li>
                                <a href="{{ url('/testimonials') }}">Testimonials</a>
                            </li>
                        </ul>
                    </nav>
                    <a href="#" class="btn">Take the Test</a>
                    <div class="user_box">
                        <img src="{{ asset('images/user_avatar.svg') }}" alt="">
                    </div>
                    <a href="" class="mob_menu_btn">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<footer class="">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="copyright">©2019 Luminary Press</div>
                <ul class="menu">
                    <li>
                        <a href="{{ url('/contact-us') }}">Contact Us</a>
                    </li>
                    <li>
                        <a href="{{ url('/terms-and-conditions') }}">Terms & Conditions</a>
                    </li>
                    <li>
                        <a href="{{ url('/privacy-policy') }}">Privacy policy</a>
                    </li>
                </ul>

                <ul class="menu_icon">
                    <li>
                        <a href="#">
                            <img src="{{ asset('images/svg/facebook.svg') }}" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{ asset('images/svg/youtube.svg') }}" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{ asset('images/svg/instagram.svg') }}" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{ asset('images/svg/twitter.svg') }}" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Popups -->

<div class="register_popup" id="login_popup">
    <h3 class="popup_title">Sign In</h3>
    <form action="{{ route('login') }}" id="loginForm" class="register_popup_form">
        @csrf
        <div class="form_group">
            <input type="email" name="email" class="text_input" placeholder="Email">
        </div>
        <div class="form_group">
            <input type="password" name="password" class="text_input" placeholder="Password">
            <div class="invalid-feedback" data-field="email"></div>
        </div>
        <div class="form_group btn_wrpr">
            <button class="btn">Sign In</button>
        </div>
    </form>
</div>

<div class="register_popup" id="register_popup">
    <h3 class="popup_title">Register</h3>
    <p class="popup_subtitle">Please register to personalize your results</p>
    <form action="{{ route('register') }}" id="registerForm" class="register_popup_form">
        @csrf
        <div class="form_group">
            <input type="text" name="name" class="text_input" placeholder="Name">
            <div class="invalid-feedback" data-field="name"></div>
        </div>
        <div class="form_group">
            <input type="email" name="email" class="text_input" placeholder="Email">
            <div class="invalid-feedback" data-field="email"></div>
        </div>
        <div class="form_group">
            <input type="password" name="password" class="text_input" placeholder="Password">
            <div class="invalid-feedback" data-field="password"></div>
        </div>
        <div class="form_group">
            <div class="select">
                <select name="gender">
                    <option value="" selected disabled>Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
            </div>
            <div class="invalid-feedback" data-field="gender"></div>
        </div>
        <div class="form_group btn_wrpr">
            <button class="btn">Continue</button>
        </div>
    </form>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>

@yield('js-section')

</body>
</html>