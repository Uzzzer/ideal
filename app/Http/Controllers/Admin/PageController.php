<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class PageController extends Controller
{

    /**
     * PageController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show page with all pages.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.pages.all-pages');
    }

    /**
     * Show page creation page.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        return view('admin.pages.add-page');
    }

}
