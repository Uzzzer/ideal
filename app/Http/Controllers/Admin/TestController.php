<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class TestController extends Controller
{

    /**
     * TestController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show page with all tests.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.tests.all-tests');
    }

}
