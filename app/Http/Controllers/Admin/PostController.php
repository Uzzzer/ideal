<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class PostController extends Controller
{

    /**
     * PostController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show page with all posts.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.posts.all-posts');
    }

    /**
     * Show page creation post.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function create()
    {
        return view('admin.posts.add-post');
    }

}
