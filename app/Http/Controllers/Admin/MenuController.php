<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class MenuController extends Controller
{

    /**
     * MenuController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show menu settings page.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.menu.menu');
    }

}
