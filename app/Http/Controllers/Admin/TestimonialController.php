<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class TestimonialController extends Controller
{

    /**
     * TestimonialController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show page with all testimonials.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.testimonials.all-testimonials');
    }

}
