<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    /**
     * SettingController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Show settings page.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function index()
    {
        return view('admin.settings.setting');
    }

}
