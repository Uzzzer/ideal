<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Support\Facades\App;

class PageController extends Controller
{

    /**
     * PageController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Page Router
     *
     * @param string $slug
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function router($slug = '')
    {
        $page = Page::where('link', $slug)->first();

        if (is_null($page) or $page->status == 'draft') {
            App::abort(404);
        }

        return view($page->template, [
            'page' => $page
        ]);
    }
}
