<?php

namespace App\Http\Controllers;

use App\Page;

class AccountController extends Controller
{

    /**
     * AccountController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Profile page.
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function profile()
    {
        return view('account.profile', [
            'page' => Page::findFromTemplate('account.profile')
        ]);
    }
}
