<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Page
 * @package App
 */
class Page extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'image', 'title', 'link', 'content', 'seo_title', 'seo_description'
    ];

    /**
     * Find page from template.
     *
     * @param $template
     * @return mixed
     */
    public static function findFromTemplate($template)
    {
        return self::where('template', $template)->first();
    }
}
